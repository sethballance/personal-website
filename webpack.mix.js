const { mix } = require('laravel-mix');
var tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (!mix.inProduction()) {
    mix.webpackConfig({
        devtool: 'source-map'
    })
        .sourceMaps()
}

mix.setPublicPath(path.normalize('./public'))
mix.js('resources/assets/js/app.js', 'js');
mix.sass('resources/assets/sass/app.sass', 'css');

mix.postCss('resources/assets/tailwind/tailwind_app.css', 'css', [
    tailwindcss('resources/assets/tailwind/tailwind.js'),
]);

mix.version();