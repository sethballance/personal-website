require('./database/database');
const express = require('express');
const app = express();
var helmet = require('helmet');
var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer();
var session = require('express-session');
var cookieParser = require('cookie-parser');
const port = 3000;
import apiRouter from './routes/api/api';
import authRouter from './routes/auth';
import projectRouter from './routes/project';
import tagRouter from "./routes/tags";

app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(upload.array());
app.use(cookieParser());
app.use(session({secret: "NotSureWhatThisIs"}));

/*Public Folder*/
app.use(express.static('./public'));

/*Routers*/
app.use('/api', apiRouter);
app.use('/auth', authRouter);
app.use('/project', projectRouter);
app.use('/tags', tagRouter);

app.get('/authenticated-user', (req, res) => {
    res.send(req.session.user);
})

/*Start Server*/
app.listen(port, function () {
    console.log(`Application running on port ${port}`);
});