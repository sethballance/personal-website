import VueRouter from 'vue-router';
import Home from '../views/home';
import About from '../views/about';
import Login from '../views/auth/login';
import Signup from '../views/auth/signup';
import Projects from '../views/projects/index';
import Project from '../views/projects/show';
import createProject from '../views/projects/create';
import editProject from '../views/projects/edit';

let routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/about',
        name: 'about',
        component: About
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/signup',
        name: 'signup',
        component: Signup
    },
    {
        path: '/projects',
        name: 'projects',
        component: Projects
    },
    {
        path: '/projects/create',
        name: 'createProject',
        component: createProject
    },
    {
        path: '/projects/:project_id',
        name: 'project',
        component: Project
    },
    {
        path: '/projects/:project_id/edit',
        name: 'editProject',
        component: editProject
    }
]

export default new VueRouter({
    routes
});