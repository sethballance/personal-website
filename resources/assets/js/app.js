import './bootstrap';
import router from './routes';

import components from './components';

window.user = {};

new Vue({
    el: '#app',
    router
});