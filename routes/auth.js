import * as query from '../database/helpers/queries';
var bcrypt = require('bcrypt');
const saltRounds = 10;
var express = require('express');
var authRouter = express.Router();

authRouter.post('/signup', (req, res) => {
    var validation = validate(req.body.username, req.body.password, req.body.confirm);

    if (validation.success) {
        bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
            query.save('users', [
                {name: 'username', value: req.body.username},
                {name: 'password', value: hash},
                {name: 'is_admin', value: false},
            ]);
        });
        res.end();
    } else {
        res.status(400).send(validation.errors);
    }
});

authRouter.post('/login', (req, res) => {
    var failureError = 'Incorrect username or password';
    authenticate(req.body.username, req.body.password).then(authentication => {
        if (authentication.user) {
            return bcrypt.compare(req.body.password, authentication.user.password).then(response => {
                if (response) {
                    req.session.user = authentication.user;
                    res.end();
                } else {
                    authentication.errors.username.push(failureError);
                    authentication.errors.password.push(failureError);
                    res.status(400).send(authentication.errors);
                }
            });
        } else {
            authentication.errors.username.push(failureError);
            authentication.errors.password.push(failureError);
            return res.status(400).send(authentication.errors);
        }
    });
});

authRouter.get('/logout', (req, res) => {
    req.session.destroy(function(){
        console.log('logged out');
    });
    res.redirect('/');
});

function validate(username, password, confirm) {
    var errors = {
        username: [],
        password: [],
        confirm: []
    }
    var success = true;

    if (!username) {
        success = false;
        errors.username.push('Please enter a username');
    }
    if (!password) {
        success = false;
        errors.password.push('Please enter a password');
    }
    if (!confirm) {
        success = false;
        errors.confirm.push('Please confirm your password');
    }
    if (password !== confirm) {
        success = false;
        const pwdError = 'Password and confirm password must be the same';
        errors.password.push(pwdError);
        errors.confirm.push(pwdError);
    }
    return {success, errors};
}

function authenticate(username, password) {
    var errors = {
        username: [],
        password: []
    }

    if (!username) {
        errors.username.push('Please enter a username');
    }
    if (!password) {
        errors.password.push('Please enter a password');
    }

    return query.get('users', `WHERE username = '${username}'`).then(user => {
        return {errors, user}
    });
}

export default authRouter;