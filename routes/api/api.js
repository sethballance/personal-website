var express = require('express');
var apiRouter = express.Router();
import projectsRouter from './projects';
import tagsRouter from './tags';

apiRouter.use('/projects', projectsRouter);
apiRouter.use('/tags', tagsRouter);

export default apiRouter;