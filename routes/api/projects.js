var express = require('express');
var projectsRouter = express.Router({mergeParams: true});
import * as queries from "../../database/helpers/queries";

projectsRouter.get('/', function (req, res) {
    queries.customQuery("select p.*, group_concat(t.name) AS tags " +
        "from projects p " +
        "left join projectTags pt on p.id = pt.project_id " +
        "left join tags t on pt.tag_id = t.id " +
        "GROUP BY p.id").then(projects => {
        projects.forEach(project => {
            if (project.tags) {
                project.tags = project.tags.split(',');
            }
        });
        res.json(projects);
    });
});

projectsRouter.get('/tagRelations', function (req, res) {
    queries.getAll('projectTags').then(projectTags => {
        res.json(projectTags);
    });
});

projectsRouter.get('/:projectId', function (req, res) {
    let query = req.params.projectId ? `WHERE id = ${req.params.projectId}` : '';

    queries.get('projects', query).then(project => {
        res.json(project);
    });
});

export default projectsRouter;