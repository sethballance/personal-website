var express = require('express');
var tagsRouter = express.Router({mergeParams: true});
import * as queries from "../../database/helpers/queries";

tagsRouter.get('/', function (req, res) {
    queries.getAll('tags').then(tags => {
        res.json(tags);
    });
});

export default tagsRouter;