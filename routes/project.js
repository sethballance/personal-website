import * as query from '../database/helpers/queries';
var express = require('express');
var projectRouter = express.Router();

projectRouter.post('/', (req, res) => {
    return query.save('projects', [
        {name: 'title', value: req.body.title},
        {name: 'description', value: req.body.description},
        {name: 'image_link', value: req.body.image_link},
        {name: 'project_link', value: req.body.project_link}
    ]).then(project_id => {
        res.json(project_id);
    });
});

export default projectRouter;