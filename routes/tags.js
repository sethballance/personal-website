import * as query from '../database/helpers/queries';
var express = require('express');
var tagRouter = express.Router();

tagRouter.post('/', (req, res) => {
    var projectTags = [];
    req.body.tags.forEach(tag => {
        var tagRow = [req.body.project_id, tag.id];
        projectTags.push(tagRow);
    });
    console.log(projectTags);
    query.saveMultiple('projectTags', ['project_id', 'tag_id'], projectTags).then(() => {
        res.end();
    });
});

export default tagRouter;