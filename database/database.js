import * as db from './helpers/queries';

export default db.build(function() {
    db.create('projects', [
        {name: 'id', type: 'INTEGER', options: 'NOT NULL PRIMARY KEY AUTOINCREMENT'},
        {name: 'title', type: 'TEXT', options: 'NOT NULL'},
        {name: 'description', type: 'TEXT', options: 'NULL'},
        {name: 'image_link', type: 'TEXT', options: 'NULL'},
        {name: 'project_link', type: 'TEXT', options: 'NULL'}
    ]);

    db.create('tags', [
        {name: 'id', type: 'INTEGER', options: 'NOT NULL PRIMARY KEY AUTOINCREMENT'},
        {name: 'name', type: 'TEXT', options: 'NOT NULL'}
    ]);

    db.create('projectTags', [
        {name: 'id', type: 'INTEGER', options: 'NOT NULL PRIMARY KEY AUTOINCREMENT'},
        {name: 'project_id', type: 'INTEGER', options: 'NOT NULL'},
        {name: 'tag_id', type: 'INTEGER', options: 'NOT NULL'}
    ]);

    db.create('users', [
        {name: 'id', type: 'INTEGER', options: 'NOT NULL PRIMARY KEY AUTOINCREMENT'},
        {name: 'username', type: 'TEXT', options: 'NOT NULL'},
        {name: 'password', type: 'TEXT', options: 'NOT NULL'},
        {name: 'is_admin', type: 'INTEGER', options: 'NULL'}
    ]);
});