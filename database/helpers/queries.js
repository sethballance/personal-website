var sqlite3 = require('sqlite3').verbose();
var path = require('path');
var moment = require('moment');
var databasePath = path.resolve('database', 'database.sqlite');
var database = new sqlite3.Database(databasePath);

export function build(callback) {
    database.serialize(callback());
    return database;
}

export function create(tableName, columns) {
    let query = `CREATE TABLE IF NOT EXISTS ${tableName} (`;

    columns.forEach((column) => {
        query = `${query} ${column.name} ${column.type} ${column.options},`;
    });

    query = `${query} created_at DATETIME NULL, updated_at DATETIME NULL)`;

    database.run(query);
}

export function save(tableName, columns) {
    let query = `INSERT INTO ${tableName} (`;

    columns.forEach((column) => {
        query = `${query}${column.name},`;
    });
    query = `${query}created_at,updated_at) VALUES (`;

    columns.forEach((column) => {
        if (typeof column.value === 'string') {
            query = `${query}'${column.value}',`
        } else {
            query = `${query}${column.value},`
        }
    });

    let now = moment().format('YYYY-MM-DD HH:mm:ss');

    query = `${query}'${now}','${now}')`;

    return new Promise((resolve, reject) => {
        database.run(query, function(error, results) {
            if (error) reject(error);
            else resolve(this.lastID);
        });
    });
}

export function saveMultiple(tableName, columns, rows) {
    let query = `INSERT INTO ${tableName} (`;

    columns.forEach((column) => {
        query = `${query}${column},`;
    });
    query = `${query}created_at,updated_at) VALUES `;

    let now = moment().format('YYYY-MM-DD HH:mm:ss');

    rows.forEach(row => {
        query = `${query}(`
        row.forEach(value => {
            if (typeof value === 'string') {
                query = `${query}'${value}',`
            } else {
                query = `${query}${value},`
            }
        });
        query = `${query}'${now}','${now}'),`;
    });

    query = query.substring(0, query.length-1);
    
    return new Promise((resolve, reject) => {
        database.run(query, function(error, results) {
            if (error) reject(error);
            else resolve(this.lastEventId);
        });
    });
}

export function edit(tableName, columns, id) {
    let query = `UPDATE ${tableName} SET`;

    columns.forEach((column) => {
        if (typeof column.value === 'string') {
            query = `${query}${column.value} = '${column.value}',`
        } else {
            query = `${query} ${column.value} = ${column.value},`
        }
    });

    let now = moment().format('YYYY-MM-DD HH:mm:ss');

    query = `${query} updated_at = '${now}' WHERE id = ${id}`;

    database.run(query);
}

export function get(tableName, filter = '', columns = null) {
    let colQuery = columns ? columns.toString() : '*';
    let query = `SELECT ${colQuery} FROM ${tableName} ${filter}`;

    return new Promise((resolve, reject) => {
        database.get(query, (error, results) => {
            if (error) reject(error);
            else resolve(results);
        });
    });
}

export function getAll(tableName, filter = '', columns = null) {
    let colQuery = columns ? columns.toString() : '*';
    let query = `SELECT ${colQuery} FROM ${tableName} ${filter}`;

    return new Promise((resolve, reject) => {
        database.all(query, (error, results) => {
            if (error) reject(error);
            else resolve(results);
        });
    });
}

export function remove(tableName, filter = '') {
    let query = `DELETE FROM ${tableName} ${filter}`;

    return new Promise((resolve, reject) => {
        database.run(query, (error, results) => {
            if (error) reject(error);
            else resolve(results);
        });
    });
}

export function customQuery(query) {
    return new Promise((resolve, reject) => {
        database.all(query, (error, results) => {
            if (error) reject(error);
            else resolve(results);
        });
    });
}